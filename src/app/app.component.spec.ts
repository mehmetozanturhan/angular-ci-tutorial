import {async, TestBed} from '@angular/core/testing';
import {AppComponent} from './app.component';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'angula-ci-tutorial'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('angula-ci-tutorial');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.content span').textContent).toContain('angula-ci-tutorial app is running!');
  });

  console.log('feature1');
  console.log('feature2');
  console.log('feature3');
  console.log('feature3 commit 2');
  console.log('fix 1');
  console.log('fix 2');
  console.log('fix 3');
  console.log('fix 4');
  console.log('fix 5');
  console.log('feature 111');
  console.log('feature 112');
  console.log('fix 100');
  console.log('feature 113');

});
